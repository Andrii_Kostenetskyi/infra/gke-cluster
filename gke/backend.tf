terraform {
  backend "gcs" {
    bucket = "docs-devops"
    prefix = "terraform/state"
  }
}