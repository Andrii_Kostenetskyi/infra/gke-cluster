Table of Contents
-----------------
1. [Description](#description)
2. [Pipeline](#pipeline)
3. [Variables](#variables)
3. [License](#license)
4. [Author](#author-information)

Description
=========

Repo provides a terraform code deploying GKE cluster to enable GitOps for k8s deployments. 
Repo also illustrates development lifecycle with terraforming that can be used in a team following strict GitOps model, 
with peers review in MR and manual final deploy trigger once code has been merged to a master.


Pipeline
------------

A multistage gitlab-ci pipeline:
- init stage validate all providers

- validate stage will check all syntax checks 

- compliance stage:
  - utilizing Open Agent Policy conftest environment via [confectionery](https://github.com/Cigna/confectionery) 
    project that provides a set of rules and policies over GKE terraform code by use of best practices. 
    Hence, this stage enables static code testing capability to comply with these policies. 
    All credits go to [confectionery](https://github.com/Cigna/confectionery) and [conftest](https://www.conftest.dev/) communities.

- Costs validation stage:
  -  [infracost](https://www.infracost.io/) tool was used to elaborate a budget/costs.   
  - as part of this job once test has successfully passed a json code MR comment (with costs estimation result) 
    is going to be created via API of gitlab
    cost_estimation.png ![](cost_estimation.png)


    
- build stage:
  - will not only run plan and in-built in terraform code validation, but also is going to save it in .json format and cache it
  - is going to generate native terraform gitlab-ci report presenting it in MR
  - is going to make API call to gitlab server creating a discussions comment attaching pre-formatted plan using gitlab access token - that will ensure a peers during MR review can directly check in UI what has been created/changed/deleted in a plan.

- deploy: 
  - only when the MR is going to be merged to master this job is going to be activated giving a final manual step to press a button to actually deploy the resources (based on gitlab RBAC and master commits ruleset - this can be user with higher privileges).

Variables
------------

Input variables used in CI/CD variables section:

|Variable Key|Description|
|------------|-----------|
|GCP_PROJECT_ID|Required for Google project id via GitLab runner|
|GOOGLE_APPLICATION_CREDENTIALS|Required for Google authentification via GitLab runner (json format which provides gcloud tool)|
|GITLAB_TOKEN|Required for pipleline costs documentation via gitlab API attaching it as commit comment|
|INFRACOST_API_KEY|Required for pipeline costs calculation (To get this key, execute and follow: infracost register)|

License
-------

GNU GPL license


Author Information
------------------

Andrii KOSTENETSKYI




